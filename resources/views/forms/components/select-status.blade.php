<x-forms::field-wrapper
    :id="$getId()"
    :label="$getLabel()"
    :label-sr-only="$isLabelHidden()"
    :helper-text="$getHelperText()"
    :hint="$getHint()"
    :hint-icon="$getHintIcon()"
    :required="$isRequired()"
    :state-path="$getStatePath()"
    class="relative"
>


    <x-forms::dropdown
        {{ $attributes->class(['']) }}
        placement="bottom-start"
    >
    @php
    ['text_color'=>$text_color,'name'=>$name,'bg_color'=>$bg_color] = $getState();
    @endphp
        <x-slot name="trigger">
            <span
                @class(['px-2 py-1 rounded-lg text-sm'])"
                style="background-color:{{$bg_color}}; color:{{$text_color}};"
            >{{$name??''}}</span>
        </x-slot>

        <x-forms::dropdown.list class="space-y-4">
            @foreach ($getStatuses() as $status)
                <x-forms::dropdown.list.item 
                :wire:click="'dispatchFormEvent(\'select-status::select\', \'' . $getStatePath() . '\', \'' . $status['id'].'\' )'"
                x-on:click="close"
                style="background-color:{{$status['bg_color']}}; color:{{$status['text_color']}};">
                    <span @class(['px-2 py-1 rounded-lg text-sm'])">
                        {{$status['name']}}
                    </span>
                </x-forms::dropdown.list.item>
            @endforeach
            {{--   @foreach ($blocks as $block)
            <x-forms::dropdown.list.item
                :wire:click="'dispatchFormEvent(\'builder::createItem\', \'' . $statePath . '\', \'' . $block->getName() . '\'' . ($createAfterItem ? ', \'' . $createAfterItem . '\'' : '') . ')'"
                :icon="$block->getIcon()"
                x-on:click="close"
            >
                {{ $block->getLabel() }}
            </x-forms::dropdown.list.item>
        @endforeach --}}
        </x-forms::dropdown.list>
    </x-forms::dropdown>
</x-forms::field-wrapper>
