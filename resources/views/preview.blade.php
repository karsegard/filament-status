<x-dynamic-component
    :component="$getFieldWrapperView()"
    :id="$getId()"
    :label="$getLabel()"
    :label-sr-only="$isLabelHidden()"
    :helper-text="$getHelperText()"
    :hint="$getHint()"
    :hint-action="$getHintAction()"
    :hint-color="$getHintColor()"
    :hint-icon="$getHintIcon()"
    :required="$isRequired()"
    :state-path="$getStatePath()"
>
    <div x-data="{ state: $wire.entangle('{{ $getStatePath() }}').defer }">
        @php
           $state = $getState();
            $name =$state['name']??'';
            $bg_color = $state['bg_color'] ?? '';
            $text_color = $state['text_color'] ?? '';
        @endphp
       <span style="background-color:{{$bg_color }}; color:{{ $text_color}};" @class(["px-2 py-1 rounded-lg text-sm"])">
            {{$name}}
        </span>
    </div>
</x-dynamic-component>