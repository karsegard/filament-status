<div x-data="{ error: undefined, pick: false }" {{ $attributes->merge($getExtraAttributes())->class(['relative']) }}
    x-on:mouseover.away=" pick = false">


    <div x-on:click.prevent.stop="pick=!pick"
        style="background-color:{{ $getBackgroundColor() }}; color:{{ $getTextColor() }};" @class([
            'px-2 py-1 text-sm cursor-pointer',
            'aspect-square rounded-md h-4 w-4' => !$getShouldDisplayLabel(),
            ' rounded-lg text-center' => $getShouldDisplayLabel(),
        ])">
        @if ($getShouldDisplayLabel())
            {{ $getState() }}
        @endif
        @if (blank($getState()))
            pick status
        @endif
        <div class=" absolute z-20 pt-2 min-w-8" x-show="pick" x-cloak>
            <div
                class="flex flex-row flex-wrap gap-2 p-4  border border-gray-300 shadow-sm bg-white rounded-xl max-w-lg">
                @foreach ($getStatuses() as $status)
                    <span
                        x-on:click.prevent.stop="
                response = await $wire.updateTableColumnState(@js($getName()), @js($recordKey), @js($status['key']))
                error = response?.error ?? undefined
                pick=false
            "
                        style="background-color:{{ $status['bg_color'] }}; color:{{ $status['text_color'] }};"
                        @class(['cursor-pointer px-2 py-1 rounded-lg text-sm'])">
                        {{ $status['name'] }}
                    </span>
                @endforeach
            </div>
        </div>
    </div>


</div>
