<div class="px-4 py-3">
    <span style="background-color:{{ $getBackgroundColor() }}; color:{{ ($getTextColor()) }};" @class(["px-2 py-1 rounded-lg text-sm"])">
        {{ $getState() }}
    </span>
</div>
