<?php

namespace KDA\Filament\Status\Concerns;

use Closure;
use Illuminate\Contracts\Support\Arrayable;
use KDA\Laravel\Status\Models\Status;

trait HasStatusGroup{
    protected string | Closure | null $statusGroup = null;
    public function statusGroup(string | Closure  $group): static
    {
        $this->statusGroup = $group;

        $this->statuses(fn () => Status::group($group)->orderBy('sort')->get()->toArray());

        return $this;
    }

    public function getStatusGroup(): string
    {
        //return $this->statusGroup;
        return $this->evaluate($this->statusGroup,[]);
    }


}