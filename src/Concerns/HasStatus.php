<?php

namespace KDA\Filament\Status\Concerns;

use Closure;
use Illuminate\Contracts\Support\Arrayable;

trait HasStatus{
    protected array | Arrayable | string | Closure | null $statuses = null;

    public function statuses(array | Arrayable | string | Closure | null $statuses): static
    {
        $this->statuses = $statuses;

        return $this;
    }

    public function getStatuses(): array
    {
        $statuses = $this->evaluate($this->statuses) ?? [];

        if ($statuses instanceof Arrayable) {
            $statuses = $statuses->toArray();
        }

        return $statuses;
    }

}