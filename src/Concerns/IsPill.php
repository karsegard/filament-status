<?php

namespace KDA\Filament\Status\Concerns;

use Closure;
use Illuminate\Contracts\Support\Arrayable;
use KDA\Laravel\Status\Models\Status;

trait IsPill{
    protected string | Closure | null $backgroundColor=null;
    protected string | Closure | null $textColor=null;

    public function setTextColor(string|Closure $color){
        $this->textColor=$color;
        return $this;
    }

    public function setBackgroundColor(string|Closure $color){
        $this->backgroundColor=$color;
        return $this;
    }

    public function getTextColor():string | null{
        return $this->evaluate($this->textColor);
    }

    public function getBackgroundColor():string | null{
        return $this->evaluate($this->backgroundColor);
    }
}