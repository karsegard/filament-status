<?php

namespace KDA\Filament\Status\Resources;

use KDA\Filament\Status\Resources\StatusResource\Pages;
use KDA\Filament\Status\Resources\StatusResource\RelationManagers;
use Filament\Forms;
use Filament\Forms\Components\ColorPicker;
use Filament\Forms\Components\Fieldset;
use Filament\Forms\Components\TagsInput;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Toggle;
use Filament\Forms\Components\View;
use Filament\Forms\Components\ViewField;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use KDA\Filament\Status\Resources\StatusResource\Pages\CreateStatus;
use KDA\Filament\Status\Tables\Columns\PillColumn;
use KDA\Laravel\Status\Models\Status;

class StatusResource extends Resource
{
    protected static ?string $model = Status::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';
    protected $listeners = ['refreshComponent' => '$refresh'];
    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                //
                TextInput::make('name')->reactive()->afterStateUpdated(function($get,$set,$livewire,$state){
                    
                    $livewire instanceof CreateStatus && $set('key',\Str::slug($get('name')))  ;
                    $set('preview.name',$state) ;
                }),

                TextInput::make('key'),
                TagsInput::make('group')->suggestions(Status::allGroups()),

                Fieldset::make('workflow')->schema([
                    Toggle::make('initial'),
                    Toggle::make('final'),
                ])->columnSpan(1),

                Fieldset::make('colors')->schema([
                    ViewField::make('preview')->view('filament-status::preview')->afterStateHydrated(function($get,$set,$state,$component){
                        $component->state([
                            'name'=>$get('name'),
                            'bg_color'=>$get('bg_color'),
                            'text_color'=>$get('text_color'),

                        ]);
                    }),
                    ColorPicker::make('bg_color')->reactive()->afterStateUpdated(fn($set,$state)=>$set('preview.bg_color',$state)),
                    ColorPicker::make('text_color')->reactive()->afterStateUpdated(fn($set,$state)=>$set('preview.text_color',$state)),
                ])->columnSpan(1)
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                //
                PillColumn::make('name')
                    ->setBackgroundColor(fn ($record) => $record->bg_color)
                    ->setTextColor(fn ($record) => $record->text_color),
                TextColumn::make('key'),
                TextColumn::make('group'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ])
            ->reorderable();
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListStatuses::route('/'),
            'create' => Pages\CreateStatus::route('/create'),
            'edit' => Pages\EditStatus::route('/{record}/edit'),
        ];
    }
}
