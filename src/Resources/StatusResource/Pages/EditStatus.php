<?php

namespace KDA\Filament\Status\Resources\StatusResource\Pages;

use KDA\Filament\Status\Resources\StatusResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditStatus extends EditRecord
{
    protected static string $resource = StatusResource::class;
    protected $listeners = ['refreshComponent' => '$refresh'];

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
