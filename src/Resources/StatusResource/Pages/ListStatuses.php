<?php

namespace KDA\Filament\Status\Resources\StatusResource\Pages;

use KDA\Filament\Status\Resources\StatusResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListStatuses extends ListRecords
{
    protected static string $resource = StatusResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
