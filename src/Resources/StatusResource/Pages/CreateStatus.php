<?php

namespace KDA\Filament\Status\Resources\StatusResource\Pages;

use KDA\Filament\Status\Resources\StatusResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateStatus extends CreateRecord
{
    protected static string $resource = StatusResource::class;
}
