<?php

namespace KDA\Filament\Status\Tables\Columns;

use Closure;
use Filament\Tables\Columns\Column;
use Filament\Tables\Columns\Contracts\Editable;
use Filament\Tables\Columns\Concerns;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use KDA\Filament\Status\Concerns\HasStatus;
use KDA\Filament\Status\Concerns\HasStatusGroup;
use KDA\Laravel\Status\Facades\Status as FacadesStatus;

class PickStatusColumn extends StatusColumn implements Editable
{
    use Concerns\CanBeValidated;
    use Concerns\CanUpdateState;
    use HasStatus;
    use HasStatusGroup;

    protected string $view = 'filament-status::tables.columns.pick_status';
    protected bool | Closure $shouldDisplayLabel =  true;

    public function shouldDisplayLabel(bool|Closure $shouldDisplayLabel): static
    {
        $this->shouldDisplayLabel = $shouldDisplayLabel;
        return $this;
    }
    public function getShouldDisplayLabel(): bool
    {
        return $this->evaluate($this->shouldDisplayLabel);
    }

  

    protected function setUp(): void
    {
        parent::setUp();

        $this->disableClick();
        $this->updateStateUsing(function($state,$record){
            FacadesStatus::setStatus($record,$state);
        });
    }
}
