<?php

namespace KDA\Filament\Status\Tables\Columns;

use Closure;
use Filament\Tables\Columns\Column;

use KDA\Laravel\Status\Facades\Status;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class StatusColumn extends PillColumn
{

    protected function setUp(): void
    {
        $name=  $this->getName();
        $this->setBackgroundColor(fn($record)=> Status::getStatus($record)?->bg_color);
        $this->setTextColor(fn($record)=> Status::getStatus($record)?->text_color);
        $this->getStateUsing(fn($record)=>Status::getStatus($record)?->name);
    }

    public function applyEagerLoading(Builder | Relation $query): Builder | Relation
    {
        
        if ($this->isHidden()) {
            return $query;
        }


        $query = parent::applyEagerLoading($query);

        return $query->with(['lastStatusHistory']);
       
    }
}
