<?php

namespace KDA\Filament\Status\Tables\Columns;

use Closure;
use Filament\Tables\Columns\Column;
use KDA\Filament\Status\Concerns\IsPill;

class PillColumn extends Column
{
    use IsPill;
    protected string $view = 'filament-status::tables.columns.pill';
    
    

    protected function setUp(): void
    {

    }
}
