<?php
namespace KDA\Filament\Status;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
use KDA\Laravel\Traits\HasConfig;
use KDA\Laravel\Traits\HasProviders;
use KDA\Laravel\Traits\HasViews;

//use Illuminate\Support\Facades\Blade;
class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasConfig;
    use HasProviders;
    use HasViews;
    protected $packageName ='filament-status';
   
    protected $configs = [
        'kda/filament-status.php' => 'kda.filament-status'
    ];
    
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
        //register filament provider
    protected $additionnalProviders=[
        \KDA\Filament\Status\FilamentServiceProvider::class
    ];
    public function register()
    {
        parent::register();
    }
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
    }
}
