<?php

namespace KDA\Filament\Status\Forms;

use Closure;
use Filament\Forms\Components\Field;
use Illuminate\Support\Collection;
use KDA\Filament\Status\Concerns\HasStatus;
use KDA\Filament\Status\Concerns\HasStatusGroup;
use KDA\Filament\Status\Concerns\IsPill;
use KDA\Laravel\Status\Facades\Status;

class SelectStatus  extends Field
{

    use HasStatus;
    use IsPill;
    use HasStatusGroup;
    protected string $view = 'filament-status::forms.components.select-status';
    protected function setUp(): void
    {
        $this->dehydrated(true);

        $this->afterStateHydrated(static function (SelectStatus $component, $record): void {
            //$component->loadState($state);
         //   $record = $component->getRecord();
            $status = Status::getStatus($record);
            if(!$status ){
                $status = Status::getDefaultStatusForModel($record,$component->getStatusGroup());
            }
            $component->state($status?->toArray());
        });

        $this->saveRelationshipsUsing(static function (SelectStatus $component, $record,$state) {
            if($record){
                $status = Status::getStatus($record);
                if($status->id!==$state['id']){
                    $s = Status::findStatusById($state['id']);
                    Status::setStatus($record,$s);
                }
            }
        });

       // $this->setBackgroundColor(fn ($record) => Status::getStatus($record)?->bg_color);
       // $this->setTextColor(fn ($record) => Status::getStatus($record)?->text_color);
        $this->registerListeners([
            'select-status::select' => [
                function (SelectStatus $component, string $statePath, $status): void {
                    if ($component->isDisabled()) {
                        return;
                    }

                    if ($statePath !== $component->getStatePath()) {
                        return;
                    }
                    $status = Status::findStatusById($status);
                    $component->state(  $status?->toArray() );
                }
            ]
        ]);
        // $this->getStateUsing(fn($record)=>Status::getStatus($record)?->name);
    }
    public function applyEagerLoading(Builder $query): Builder
    {
        if ($this->isHidden()) {
            return $query;
        }


        $query = parent::applyEagerLoading($query);

        /* if ($this->queriesRelationships($query->getModel())) {
            $query->with([$this->getRelationshipName()]);
        }*/
        return $query->with(['lastStatusHistory']);
        //return $query;
    }
}
