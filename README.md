# This is my package filament-status

[![Latest Version on Packagist](https://img.shields.io/packagist/v/kda/filament-status.svg?style=flat-square)](https://packagist.org/packages/kda/filament-status)
[![Total Downloads](https://img.shields.io/packagist/dt/kda/filament-status.svg?style=flat-square)](https://packagist.org/packages/kda/filament-status)

## Installation

You can install the package via composer:

```bash
composer require kda/filament-status
```

You can publish and run the migrations with:

```bash
php artisan vendor:publish --provider="\KDA\Filament\Status\ServiceProvider" --tag="migrations"
php artisan migrate
```

You can publish the config file with:

```bash
php artisan vendor:publish --provider="\KDA\Filament\Status\ServiceProvider" --tag="config"
```

This is the contents of the published config file:

```php
return [
];
```

Optionally, you can publish the views using

```bash
php artisan vendor:publish --provider="\KDA\Filament\Status\ServiceProvider" --tag="views"
```

## Usage

```php
$filament\Status = new KDA\Filament\Status();
echo $filament\Status->echoPhrase('Hello, KDA!');
```

## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](https://github.com/fdt2k/.github/blob/main/CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](../../security/policy) on how to report security vulnerabilities.

## Credits

- [Fabien Karsegard](https://github.com/fdt2k)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
